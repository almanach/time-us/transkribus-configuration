# Transkribus : Configuration

Ce dossier contient le fichier config.properties permettant de faciliter l'homogénéisation de la configuration de Transkribus pour tous les membres du projet.  

Cette configuration vise notamment à faciliter la création des *tags* d'annotation dans Transkribus.

> config.properties est actuellement basé sur une copie du fichier du même nom tel qu'il a été conçu pour Transkribus 1.4. Il n'a pas été validé pour les versions ultérieures.

## Documentation supplémentaire : 

- [Guide pour l'installation](http://timeusage.paris.inria.fr/mediawiki/index.php/Guide_pour_l%27installation_de_la_liste_des_tags_Time_Us)
- [Rapport interne sur l'annotation (juin 2018)](https://gitlab.inria.fr/almanach/time-us/transkribus-configuration/wikis/Rapport-sur-l'annotation-(juin-2018))
